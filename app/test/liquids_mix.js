var expect = require('expect.js');
var liquids = require('../src/liquids');

function check(input, output){
  var body = liquids.parseMix(input);
  expect(output).to.eql(body);
}

describe('parse liquids mix', function(){
  it('expects to get only mix', function(){
    var input = [
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500}
    ];
    check(input, {"JAS_GRDA": 1000});
  });
  it('expects to run out of mix', function(){
    var input = [
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500}
    ];

    check(input, {"JAS_GRDA": 2000, "JAS": 700, "GRDA": 300});
  });
  it('expects to not get almond', function(){
    var input = [
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine_almond", "liquid": 500}
    ];
    check(input,  {"JAS_GRDA": 2000, "JAS_ALMD": 500, "JAS": 700, "GRDA": 300});
  });
  it('expects to get everything', function(){
    var input = [
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine_almond", "liquid": 500},
      {"name": "Jasmine_almond", "liquid": 500},
      {"name": "Jasmine_almond", "liquid": 500},
      {"name": "Matcha_almond", "liquid": 500},
      {"name": "Matcha_almond", "liquid": 500},
      {"name": "Matcha_almond", "liquid": 500}
    ];
    check(input,   {"JAS_GRDA": 2000, "JAS_ALMD": 1000, "JAS": 1050, "MAT_ALMD": 1000, "MAT": 350, "GRDA": 300, "ALMD": 300});
  });
});