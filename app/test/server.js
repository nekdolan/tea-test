var expect = require('expect.js');
var server = require('../src/server');
var request = require('request');

var port = 8081;
var url = 'http://localhost:'+port;

function check(endpoint, input, output, done){
  request.post({url: url+'/'+endpoint, json:true, body:input},
    function(err, res, body){
      if(err){
        done(err);
        return;
      }
      try {
        expect(output).to.eql(body);
        done();
      } catch (e){
        done(e);
      }
    }
  );
}

describe('test compute_usage', function(){
  before(function(done){
    server(port, done);
  });
  describe('parse liquid', function(){
    it('expects to have ratio of 70% to 30%', function(done){
      var input = [
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500}
      ];
      check('compute_usage', input, {"JAS": 700, "GRDA": 300}, done);
    });
    it('expects to have several tea flavor', function(done){
      var input = [
        {"name": "Jasmine", "liquid": 500},
        {"name": "Matcha", "liquid": 400},
        {"name": "Jasmine", "liquid": 500}
      ];
      check('compute_usage', input, {"JAS": 700, "MAT": 280, "GRDA": 420}, done);
    });
    it('expects to have almond milk', function(done){
      var input = [
        {"name": "Jasmine", "liquid": 500},
        {"name": "Matcha", "liquid": 400},
        {"name": "Matcha_almond", "liquid": 400},
        {"name": "Jasmine", "liquid": 500}
      ];
      check('compute_usage', input, {"JAS": 700, "MAT": 560, "GRDA": 420, "ALMD": 120}, done);
    });
  });

  describe('parse liquid mix', function(){
    it('expects to get only mix', function(done){
      var input = [
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500}
      ];
      check('compute_usage_mix', input, {"JAS_GRDA": 1000}, done);
    });
    it('expects to run out of mix', function(done){
      var input = [
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500}
      ];

      check('compute_usage_mix', input, {"JAS_GRDA": 2000, "JAS": 700, "GRDA": 300}, done);
    });
    it('expects to not get almond', function(done){
      var input = [
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine_almond", "liquid": 500}
      ];
      check('compute_usage_mix', input, {"JAS_GRDA": 2000, "JAS_ALMD": 500, "JAS": 700, "GRDA": 300}, done);
    });
    it('expects to get everything', function(done){
      var input = [
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine", "liquid": 500},
        {"name": "Jasmine_almond", "liquid": 500},
        {"name": "Jasmine_almond", "liquid": 500},
        {"name": "Jasmine_almond", "liquid": 500},
        {"name": "Matcha_almond", "liquid": 500},
        {"name": "Matcha_almond", "liquid": 500},
        {"name": "Matcha_almond", "liquid": 500}
      ];
      check('compute_usage_mix', input, {"JAS_GRDA": 2000, "JAS_ALMD": 1000, "JAS": 1050,
        "MAT_ALMD": 1000, "MAT": 350, "GRDA": 300, "ALMD": 300}, done);
    });
  });
});
