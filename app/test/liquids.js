var expect = require('expect.js');
var liquids = require('../src/liquids');

function check(input, output){
  var body = liquids.parse(input);
  expect(output).to.eql(body);
}

describe('parse liquids', function(){
  it('expects to have ratio of 70% to 30%', function(){
    var input = [
      {"name": "Jasmine", "liquid": 500},
      {"name": "Jasmine", "liquid": 500}
    ];
    check(input, {"JAS": 700, "GRDA": 300});
  });
  it('expects to have several tea flavor', function(){
    var input = [
      {"name": "Jasmine", "liquid": 500},
      {"name": "Matcha", "liquid": 400},
      {"name": "Jasmine", "liquid": 500}
    ];
    check(input,  {"JAS": 700, "MAT": 280, "GRDA": 420});
  });
  it('expects to have almond milk', function(){
    var input = [
      {"name": "Jasmine", "liquid": 500},
      {"name": "Matcha", "liquid": 400},
      {"name": "Matcha_almond", "liquid": 400},
      {"name": "Jasmine", "liquid": 500}
    ];
    check(input,  {"JAS": 700, "MAT": 560, "GRDA": 420, "ALMD": 120});
  });

});