//Use ES5 for sake of consistency with client

var express = require('express');
var bodyParser = require('body-parser');
var join = require('path').join;
var liquid = require('./liquids');
var app = express();

app.use(bodyParser.json());
app.use(express.static(join(__dirname,'../frontend/static/')));
app.set('views', './app/views');
app.set('view engine', 'pug');

app.get('/', function(req, res) {
  res.render('index', {});
});

app.get('/compute_usage', function(req, res) {
  res.render('compute_usage', {});
});
app.post('/compute_usage', function(req, res) {
  var liquids = liquid.parse(req.body);
  res.send(liquids);
});

app.get('/compute_usage_mix', function(req, res) {
  res.render('compute_usage_mix', {});
});
app.post('/compute_usage_mix', function(req, res) {
  var liquids = liquid.parseMix(req.body);
  res.send(liquids);
});


function start(serverPort, cb){
  var port = serverPort || 80;
  app.listen(port, function(){
    console.info('Tea App started on port: '+port);
    if(cb){
      cb();
    }
  })
}

module.exports = start;