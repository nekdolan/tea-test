var _ = require('lodash');

var C = {
  Jasmine : 'Jasmine',  Matcha : 'Matcha',
  _almond : '_almond',  almond : 'almond',
  JAS : 'JAS',  MAT: 'MAT',  GRDA: 'GRDA',  ALMD: 'ALMD',
  JAS_GRDA: 'JAS_GRDA', JAS_ALMD : 'JAS_ALMD',
  MAT_GRDA: 'MAT_GRDA', MAT_ALMD : 'MAT_ALMD'
};

function filterTea(teaCollection){
  var teaNames = [C.Jasmine, C.Matcha, C.Jasmine+C._almond, C.Matcha+C._almond];
  var filteredTeaCollection = _.filter(teaCollection, function(tea){
    return tea && _.includes(teaNames, tea.name) &&
      _.isInteger(tea.liquid) && tea.liquid > 0;
  });
  return _.sortBy(filteredTeaCollection, 'name');
}

function parse(teaCollection, inventory, liquidsMix){
  var teaAbr = _.fromPairs([[C.Jasmine, C.JAS], [C.Matcha, C.MAT]]);
  var liquids = _.fromPairs([[C.JAS, 0], [C.MAT, 0], [C.GRDA, 0], [C.ALMD, 0]]);
  var filteredTeaCollection = inventory ? teaCollection : filterTea(teaCollection);
  _.forEach(filteredTeaCollection, function(tea){
    var names = _.split(tea.name, '_');
    var teaName = teaAbr[names[0]];
    var teaAmount = Math.floor(tea.liquid * 0.7);
    var milkName = names[1] == C.almond ? C.ALMD : C.GRDA;
    var milkAmount = tea.liquid - teaAmount;
    if(inventory){
      var teaInventory = inventory[teaName];
      var milkInventory = inventory[milkName];
      if(teaAmount > teaInventory){
        teaAmount = teaInventory;
        milkAmount = Math.floor(teaAmount*3/7);
      }
      if(milkAmount > milkInventory){
        teaAmount = Math.floor(milkInventory*7/3);
        milkAmount = milkAmount - milkInventory;
      }
      inventory[teaName] -= teaAmount;
      inventory[milkName] -= milkAmount;
    }
    liquids[teaName] += teaAmount;
    liquids[milkName] += milkAmount;
  });
  if(liquidsMix){
    _.assign(liquids, liquidsMix);
  }
  _.forIn(liquids, function(value, key){
    if(!value){
      delete liquids[key];
    }
  });
  return liquids;
}

function parseMix(teaCollection){
  var mixAbr = _.fromPairs([[C.Jasmine, C.JAS_GRDA], [C.Matcha, C.MAT_GRDA],
    [C.Jasmine+C._almond, C.JAS_ALMD], [C.Matcha+C._almond, C.MAT_ALMD]
  ]);
  var inventory = _.fromPairs([
    [C.JAS, 2000], [C.JAS_GRDA,2000], [C.JAS_ALMD, 1000],
    [C.MAT, 2000], [C.MAT_GRDA, 2000], [C.MAT_ALMD, 1000],
    [C.GRDA, 1000], [C.ALMD, 1000]
  ]);
  var liquids = _.fromPairs([[C.JAS_GRDA, 0], [C.JAS_ALMD, 0], [C.MAT_GRDA, 0], [C.MAT_ALMD, 0]]);
  var filteredTeaCollection = filterTea(teaCollection);
  var nomix = [];
  _.forEach(filteredTeaCollection, function(tea){
    var name = tea.name;
    var abrName = mixAbr[name];
    var mixTeaInventory = inventory[abrName];
    var reduceLiquidBy = tea.liquid;
    if(mixTeaInventory < reduceLiquidBy){
      var nomixTea = _.clone(tea);
      nomixTea.liquid -= mixTeaInventory;
      reduceLiquidBy = mixTeaInventory;
      nomix.push(nomixTea);
    }
    liquids[abrName] += reduceLiquidBy;
    inventory[abrName] -= reduceLiquidBy;
  });
  return parse(nomix, inventory, liquids);
}

module.exports = {
  constants : _.clone(C),
  parse : parse,
  parseMix : parseMix
};