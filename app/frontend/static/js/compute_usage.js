var $rowContainer = $('#row-container');
var $addRow = $('#add-row');
var rowElementHtml = $('#row-element').html();
var $mainForm = $('#main-form');
var $resultTable = $('#result-table');

$addRow.click(function(){
  $rowContainer.append(rowElementHtml);
});

$mainForm.submit(function(event){
  event.preventDefault();
  var rowValues = $mainForm.serializeArray();
  var resultArr = _.map(_.chunk(rowValues, 3), function(row){
    return {
      name : row[0].value + row[1].value,
      liquid : +row[2].value
    }
  });

  $.ajax({
    url: '',
    type: 'POST',
    data: JSON.stringify(resultArr),
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',
    success: function(data) {
      $resultTable.find('td').html('&nbsp;');
      _.forIn(data, function(value, key){
        $('#'+key).html(value);
      });
    }
  });

});